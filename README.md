Damask is an automatic wallpaper application. It currently supports setting the
wallpaper image from the following sources:

* wallhaven.cc
* Microsoft Bing Wallpaper of the day
* NASA Astronomy Picture of the Day[^1]
* Unsplash[^1]

![application screenshot](./data/damask.png)

[^1]: Planned
